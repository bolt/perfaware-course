/* SPDX-License-Identifier: GPL-2.0-only */
#ifndef SIMDISASM_H
#define SIMDISASM_H

#include "simpool.h"
#include "simcommon.h"


/*
 * NOTE: Commented out opcodes and many others have yet to be implemented
 */
enum opcode {
    OP_INVALID,
    /* MOV instructions */
    OP_MOV_REG_RM,
    //MOV_RM_SEG,
    //MOV_SEG_RM,
    OP_MOV_ACC_MEM,
    OP_MOV_MEM_ACC,
    OP_MOV_REG_IMM,
    OP_MOV_RM_IMM,
    /* ADD, SUB, CMP instructions */
    OP_ADD_REG_RM,
    OP_ADD_ACC_IMM,
    OP_SUB_REG_RM,
    OP_SUB_ACC_IMM,
    OP_CMP_REG_RM,
    OP_CMP_ACC_IMM,
    OP_ASC_RM_IMM,  // ADD, SUB and CMP differentiate on the 2nd byte of this instruction
    /* Conditional Jump instructions */
    OP_JE,  OP_JL,  OP_JLE, OP_JB,  OP_JBE, OP_JP,
    OP_JO,  OP_JS,
    OP_JNE, OP_JNL, OP_JG,  OP_JNB, OP_JA,  OP_JNP,
    OP_JNO, OP_JNS,
    OP_JCXZ,
    OP_LOOP, OP_LOOPZ, OP_LOOPNZ,
    /* Shift / Rotate instructions */
    //SH_RO,
    /* NOP */
    OP_NOP,
};

enum inst_type {
    MOV,
    ADD, SUB, CMP,
    JE,  JL,  JLE, JB,  JBE, JP,
    JO,  JS,
    JNE, JNL, JG,  JNB, JA,  JNP,
    JNO, JNS,
    JCXZ,
    LOOP, LOOPZ, LOOPNZ,
    NOP,
};

/*
 * NULOP:   Null Operand; should never be used?
 * REG:     Register id
 * MEM:     Memory Effective Address id
 * IMM:     Immediate value
 * DRA:     Direct Address
 * ADR:     Address  // TODO: Is there any reason to distinguish this from DRA?
 * EXP:     Explicit immediate value
 */
enum operand_type {
    NULOP, REG, MEM, IMM, DRA, ADR, EXP, INC8,
};

typedef struct operand_t operand_t;
struct operand_t {
    enum operand_type type;
    union {
        u8  idx;        // REG/MEM: string array index
        s8  inc8;       // Conditional Jump/Loop
        u16 data;       // IMM/EXP: immediate value
    };
    union {
        u16 disp;       // MEM/DRA/ADR: displacement value
        u8 is_word;     // EXP: boolean; word or byte
    };
};
#define IS_NULL_OPERAND(operand) ((operand).type == NULOP)

/*
 * Intermediate form of a disassembled instruction
 *
 * Both dst and src may be empty
 * next allows us to form an linked list of instructions
 */
typedef struct inst_dis_t inst_dis_t;
struct inst_dis_t {
    enum inst_type type;
    struct operand_t dst;
    struct operand_t src;
    struct inst_dis_t *next;
};

typedef struct simstate_t simstate_t;
struct simstate_t {
    inst_dis_t   current_instruction;
    inst_dis_t  *first_instruction;
    inst_dis_t  *last_instruction;
    u8          *bytecode_at;
    u8          *bytecode_end;
    pool_t      *pool;
};


inst_dis_t *disassemble(bytebuf_t *binbuf, pool_t *pool);
void print_disassembly(inst_dis_t *instructions, char *binname);
void print_operand(operand_t *o);


#endif /* SIMDISASM_H */
