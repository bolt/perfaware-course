/*
 * This is intended to be used as part of your build.
 * It generates and outputs opcode_lut in a hardcoded format for direct
 * use as an #include.
 *
 * When executed, the file 'opcode_lut.c' is output into the cwd.
 */

#include <stdio.h>

// TODO: Add ../ to include paths so we can remove it here
#include "../simdisasm.h"
#include "../simcommon.h"

static u8 opcode_lut[256];

void
register_opcode(u8 pattern, u8 len, enum opcode opcode)
{
    u8 num_variations = 1 << (8 - len);

    for (u8 i = 0; i < num_variations; i++)
    {
        u8 pos = pattern | i;
        opcode_lut[pos] = opcode;
    }
}

/*
 * All opcodes, regardless of their length have been given a hex value as
 * though they are all 8-bit, with any flag bits simply set to zero.
 *
 * Opcodes are identified by a varying number of bits (between 4 and 8).
 * We have grouped by type.
 *
 * For reference, we also note their lengths, which should tally up against
 * the switch statements we use to determine each opcode during disassembly.
 */
void
generate_table(void)
{
    /* MOV */
    register_opcode(0x88, 6, OP_MOV_REG_RM );  // 0b100010dw
    //register_opcode(0x8C, 8, OP_MOV_RM_SEG );  // 0b10001100
    //register_opcode(0x8E, 8, OP_MOV_SEG_RM );  // 0b10001110
    register_opcode(0xA0, 7, OP_MOV_ACC_MEM);  // 0b1010000w
    register_opcode(0xA2, 7, OP_MOV_MEM_ACC);  // 0b1010001w
    register_opcode(0xB0, 4, OP_MOV_REG_IMM);  // 0b1011wreg
    register_opcode(0xC6, 7, OP_MOV_RM_IMM );  // 0b1100011w
    /* ADD, SUB, CMP */
    register_opcode(0x00, 6, OP_ADD_REG_RM );  // 0b000000dw
    register_opcode(0x04, 7, OP_ADD_ACC_IMM);  // 0b0000010w
    register_opcode(0x28, 6, OP_SUB_REG_RM );  // 0b001010dw
    register_opcode(0x2C, 7, OP_SUB_ACC_IMM);  // 0b0010110w
    register_opcode(0x38, 6, OP_CMP_REG_RM );  // 0b001110dw
    register_opcode(0x3C, 7, OP_CMP_ACC_IMM);  // 0b0011110w
    // ADD, SUB and CMP differentiate on the 2nd byte of this instruction
    register_opcode(0x80, 6, OP_ASC_RM_IMM );  // 0b100000sw
    /* Conditional Jumps */
    register_opcode(0x74, 8, OP_JE    );       // 0b01110100
    register_opcode(0x7C, 8, OP_JL    );       // 0b01111100
    register_opcode(0x7E, 8, OP_JLE   );       // 0b01111110
    register_opcode(0x72, 8, OP_JB    );       // 0b01110010
    register_opcode(0x76, 8, OP_JBE   );       // 0b01110110
    register_opcode(0x7A, 8, OP_JP    );       // 0b01111010
    register_opcode(0x70, 8, OP_JO    );       // 0b01110000
    register_opcode(0x78, 8, OP_JS    );       // 0b01111000
    register_opcode(0x75, 8, OP_JNE   );       // 0b01110101
    register_opcode(0x7D, 8, OP_JNL   );       // 0b01111101
    register_opcode(0x7F, 8, OP_JG    );       // 0b01111111
    register_opcode(0x73, 8, OP_JNB   );       // 0b01110011
    register_opcode(0x77, 8, OP_JA    );       // 0b01110111
    register_opcode(0x7B, 8, OP_JNP   );       // 0b01111011
    register_opcode(0x71, 8, OP_JNO   );       // 0b01110001
    register_opcode(0x79, 8, OP_JNS   );       // 0b01111001
    register_opcode(0xE3, 8, OP_JCXZ  );       // 0b11100011
    register_opcode(0xE2, 8, OP_LOOP  );       // 0b11100010
    register_opcode(0xE1, 8, OP_LOOPZ );       // 0b11100001
    register_opcode(0xE0, 8, OP_LOOPNZ);       // 0b11100000
    /* NOP */
    register_opcode(0x90, 8, OP_NOP   );       // 0b11100000
}

void
export_table(FILE *outfile)
{
    fprintf(outfile,
            "/*\n"
            " * Designed to work with values for enum opcode\n"
            " * Relies on a suitable definition for u8\n"
            " * This must only be included once\n"
            " */\n"
    );

    fprintf(outfile, "static u8 opcode_lut[256] = {");

    for (int i = 0; i < 256; i++)
    {
        if (! (i % 16))
            fprintf(outfile, "\n    ");
        fprintf(outfile, "0x%.2X, ", opcode_lut[i]);
    }

    fprintf(outfile, "\n};\n");
}

int
main (void)
{
    char *outname = "opcode_lut.c";

    FILE *outfile = fopen(outname, "w");
    if (! outfile)
    {
        LOG_ERROR("Failed to create output file. Aborting");
        return -1;
    }

    generate_table();

    export_table(outfile);

    return 0;
}
