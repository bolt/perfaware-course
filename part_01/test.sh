#!/bin/sh

BOLD="\e[1m"
GREEN="\e[32m"
RED="\e[31m"
RESET="\e[0m"

main() {
    APP=$1
    shift
    FILES="$@"

    if [ $# -lt 2 ]; then
        exit 1
    fi

    for f in ${FILES}; do
        echo "Test: ${f}"

        # We allow this command to use stdout and stderr
        # It should never fail unless we have bad sample case data
        echo -e "\tAssembling sample"
        nasm ${f} -o /tmp/${APP}_baseref
        if [ $? -ne 0 ]; then continue; fi

        run_test "Disassembly" "./${APP} /tmp/${APP}_baseref > /tmp/${APP}_simdisasm"
        if [ $? -ne 0 ]; then continue; fi

        run_test "Reassembly" nasm /tmp/${APP}_simdisasm -o /tmp/${APP}_simref
        if [ $? -ne 0 ]; then continue; fi

        run_test "Diff" diff -q /tmp/${APP}_baseref /tmp/${APP}_simref
        if [ $? -ne 0 ]; then continue; fi

        echo -e "\t---------------------------"
        echo -e "\t${BOLD}${GREEN}FULL PASS${RESET}"
    done

    rm -f /tmp/${APP}_*
}

run_test() {
    local msg=$1
    local fill_len=$((21 - ${#msg}))
    shift
    local cmd="$@"

    echo -ne "\t$msg "
    printf "%*s" "$fill_len" | tr ' ' '.'
    echo -n " "

    eval "$cmd" >/dev/null 2>/dev/null
    if [ $? -ne 0 ]; then
        echo -e "${RED}FAIL${RESET}"
        return 1
    fi
    echo -e "${GREEN}PASS${RESET}"
    return 0
}

main "$@"
