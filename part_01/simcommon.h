/* SPDX-License-Identifier: GPL-2.0-only */
#ifndef SIMCOMMON_H
#define SIMCOMMON_H

#include <stddef.h>

/* ========================================================================== */

// Reduce overload from keyword static
#define internal static

// Not ideal but reasonable for our current use-case
typedef unsigned char  b8;
typedef unsigned char  u8;
typedef unsigned short u16;
typedef          char  s8;
typedef          short s16;

#define MB(amount) (amount * 1024L * 1024L)

#define MAX_STRING_LEN 64

#define LOG(T, ...)                                                     \
        fprintf(stderr, "[" T "] "                                      \
                HEAD(__VA_ARGS__) "%.0d" "\n",                          \
                TAIL(__VA_ARGS__))
#define LOG_DEBUG(...) LOG("DEBUG",   __VA_ARGS__)
#define LOG_ERROR(...) LOG("ERROR",   __VA_ARGS__)
#define LOG_WARN(...)  LOG("WARNING", __VA_ARGS__)
#define LOG_INFO(...)  LOG("INFO",    __VA_ARGS__)

#ifndef NDEBUG
#define ASSERT(cond, ...)                                               \
    do {                                                                \
        if (!(cond)) {                                                  \
            fprintf(stderr, "[ASSERT] %s:%s:" STRIFY(__LINE__) " "      \
                    STRIFY((cond))  " " HEAD(__VA_ARGS__) "%.0d" "\n",  \
                    __FILE__, __func__,                                 \
                    TAIL(__VA_ARGS__));                                 \
            *(volatile int *)0 = 0;                                     \
        }                                                               \
    } while (0)
#else
#define ASSERT(...)
#endif

#define STRIFY(s) STRINGIFY(s)
#define STRINGIFY(s) #s

#define HEAD(...) HEAD0(__VA_ARGS__, 0)
#define HEAD0(_0, ...) _0
#define TAIL(...) TAIL0(__VA_ARGS__, 0)
#define TAIL0(_0, ...) __VA_ARGS__

/* ========================================================================== */

typedef struct textbuf_t textbuf_t;
struct textbuf_t {
    char *text;
    size_t sz;
};
#define NULL_TEXT_BUF(txtbuf) (! (txtbuf) || ! ((txtbuf)->text))

typedef struct bytebuf_t bytebuf_t;
struct bytebuf_t {
    u8 *bytes;
    size_t sz;
};
#define NULL_BYTE_BUF(bytebuf) (! (bytebuf) || ! ((bytebuf)->bytes))


#endif /* SIMCOMMON_H */
