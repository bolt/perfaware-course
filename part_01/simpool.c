#include "simpool.h"

#include <stdio.h>
#include <stdlib.h>

#include "simcommon.h"

/*
 * Returns null pool_t on error
 */
pool_t
pool_alloc(size_t sz)
{
    pool_t pool = { .mem = malloc(sz) };

    if (! pool.mem)
        return pool;

    pool.top = pool.mem + sz;
    pool.at = pool.mem;

    return pool;
}

void
pool_release(pool_t *pool)
{
    if (NULL_POOL(pool))
        return;
    free(pool->mem);
    *pool = (pool_t){};
}

void *
pool_push(size_t sz, pool_t *pool)
{
    ASSERT(pool->at + sz <= pool->top);

    void *ret = pool->at;
    pool->at += sz;
    return ret;
}

