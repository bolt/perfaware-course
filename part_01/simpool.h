/* SPDX-License-Identifier: GPL-2.0-only */
#ifndef SIMPOOL_H
#define SIMPOOL_H

#include <stddef.h>

#include "simcommon.h"

typedef struct pool_t pool_t;
struct pool_t {
    u8 *mem;
    u8 *top;
    u8 *at;
};
#define NULL_POOL(pool) (! (pool)->mem)

pool_t pool_alloc(size_t n);
void pool_release(pool_t *pool);
void *pool_push(size_t sz, pool_t *pool);
#define POOL_PUSH(N, pool) pool_push(N, pool)
#define POOL_PUSH_STRUCT(T, pool) pool_push(sizeof(T), pool)

#endif /* SIMPOOL_H */
