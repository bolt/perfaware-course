# Part 01

## Instruction Decoding on the 8086

Download homework data from:
[here](https://github.com/cmuratori/computer_enhance/tree/main/perfaware/part1)

### Homework 1 - Decoding `mov`

Write a disassembler that can decode the `mov` instruction.

Successfully disassemble listings `37 and 38`.

### Homework 2 - Decoding more `mov`s

Successfully disassemble listing `39`, and the optional challenge `40`.

### Homework 3 - Decoding `add`, `sub`, `cmp` and conditional `jumps`

Successfully disassemble listing `41`, and the optional, diabolical challenge `42`.

### Compilation and Testing

```sh
$ ./make
```

The `make` recipe `test` runs `test.sh` on all `.asm` files in the directory.

```sh
$ ./make test
```
