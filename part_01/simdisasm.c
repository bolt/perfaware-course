#include "simdisasm.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "simcommon.h"

/* ========================================================================== */

#include "opcode_lut/opcode_lut.c"

// pos is the zero-indexed bit position; zero being the LSB
// IMPORTANT: This will silently break if n is larger than pos + 1
#define BITS(n, pos, byte)  (~(0xff << (n)) & ((byte) >> ((pos) - (n) + 1)))
#define REG(pos, byte)      BITS(3, pos, byte)

// To determine certain opcodes we need to look at 3 bits as from position 5
// in the second byte
#define SEC_OP(byte)        BITS(3, 5, byte)

#define BIT(n, byte) ((byte) >> (n) & 1)
#define MOD(byte)    ((byte) >> 6)
#define RM(byte)     ((byte)        & 7)

#define DBIT(byte)   BIT(1, byte)
#define WBIT(byte)   BIT(0, byte)
#define SBIT(byte)   BIT(1, byte)

const u8 DIRECT_ADDRESS = 0x6;  // 0b110

enum MODE {
    MOD_NODISP = 0x0,   // 0b00
    MOD_DISP8  = 0x1,   // 0b01
    MOD_DISP16 = 0x2,   // 0b10
    REG_MODE   = 0x3,   // 0b11
};

/* ========================================================================== */

/* Prototypes */

internal void add_instruction(simstate_t *state);
internal void op_reg_rm(simstate_t *state);
internal void mov_reg_imm(simstate_t *state);
internal void mov_rm_imm(simstate_t *state);
internal void mov_acc_to_from_mem(simstate_t *state);
internal void asc_rm_imm(simstate_t *state);
internal void asc_acc_imm(simstate_t *state);
internal void conditional_jump(simstate_t *state);
internal void invalid_opcode_abort(u8 opcode);
internal void handle_memory_operand(simstate_t *state, u8 mod, u8 mem, operand_t *out);

internal inline b8  bytes_remain(simstate_t *state);
internal inline u8  peek_u8(simstate_t *state);
internal inline u8  consume_u8(simstate_t *state);
internal inline s8 consume_s8(simstate_t *state);
internal inline u16 consume_u16(simstate_t *state);
internal inline s16 consume_s16(simstate_t *state, b8 should_sign_extend, b8 should_shift_hi_byte);

/* ========================================================================== */

/* File Globals */

#define REGIDX(i, w) ((i) | ((w) << 0x3))
static textbuf_t reg_strings[] = {
    // 8-bit
    {"al", 2}, {"cl", 2}, {"dl", 2}, {"bl", 2},
    {"ah", 2}, {"ch", 2}, {"dh", 2}, {"bh", 2},
    // 16-bit
    {"ax", 2}, {"cx", 2}, {"dx", 2}, {"bx", 2},
    {"sp", 2}, {"bp", 2}, {"si", 2}, {"di", 2}
};
static textbuf_t ea_strings[] = {
    {"bx + si", 7}, {"bx + di", 7}, {"bp + si", 7}, {"bp + di", 7},
    {"si", 2},      {"di", 2},      {"bp", 2},      {"bx", 2}
};
/*
 * These must directly reflect enum inst_type
 */
static textbuf_t mnemonics[] = {
    { "mov", 3 }, { "add", 3 }, { "sub", 3 }, { "cmp", 3},
    { "je", 2 }, { "jl", 2 }, { "jle", 3 }, { "jb", 2}, { "jbe", 3}, { "jp", 2},
    { "jo", 2}, { "js", 2},
    { "jne", 3 }, { "jnl", 3 }, { "jg", 2 }, { "jnb", 3}, { "ja", 2}, { "jnp", 3},
    { "jno", 3}, { "jns", 3},
    { "jcxz", 4},
    { "loop", 4}, { "loopz", 5}, { "loopnz", 6},
    { "nop", 3},
};

/* ========================================================================== */

/* Function Definitions */

/*
 * IMPORTANT: pool is assumed to be thread-safe
 */
inst_dis_t *
disassemble(bytebuf_t *binbuf, pool_t *pool)
{
    simstate_t state = {
        .bytecode_at = binbuf->bytes,
        .bytecode_end = binbuf->bytes + binbuf->sz,
        .pool = pool,
    };

    while (bytes_remain(&state))
    {
        u8 instruction_byte = peek_u8(&state);
        enum opcode opcode = opcode_lut[instruction_byte];

        switch (opcode)
        {
        case OP_MOV_REG_RM: {
            state.current_instruction.type = MOV;
            op_reg_rm(&state);
        } break;

        case OP_MOV_ACC_MEM: /* fall through */
        case OP_MOV_MEM_ACC: {
            state.current_instruction.type = MOV;
            mov_acc_to_from_mem(&state);
        } break;

        case OP_MOV_REG_IMM: {
            state.current_instruction.type = MOV;
            mov_reg_imm(&state);
        } break;

        case OP_MOV_RM_IMM: {
            state.current_instruction.type = MOV;
            mov_rm_imm(&state);
        } break;

        case OP_ADD_REG_RM: {
            state.current_instruction.type = ADD;
            op_reg_rm(&state);
        } break;

        case OP_ADD_ACC_IMM: {
            state.current_instruction.type = ADD;
            asc_acc_imm(&state);
        } break;

        case OP_SUB_REG_RM: {
            state.current_instruction.type = SUB;
            op_reg_rm(&state);
        } break;

        case OP_SUB_ACC_IMM: {
            state.current_instruction.type = SUB;
            asc_acc_imm(&state);
        } break;

        case OP_CMP_REG_RM: {
            state.current_instruction.type = CMP;
            op_reg_rm(&state);
        } break;

        case OP_CMP_ACC_IMM: {
            state.current_instruction.type = CMP;
            asc_acc_imm(&state);
        } break;

        case OP_ASC_RM_IMM: {
            asc_rm_imm(&state);
        } break;

        case OP_JE: {
            state.current_instruction.type = JE;
            conditional_jump(&state);
        } break;

        case OP_JL: {
            state.current_instruction.type = JL;
            conditional_jump(&state);
        } break;

        case OP_JLE: {
            state.current_instruction.type = JLE;
            conditional_jump(&state);
        } break;

        case OP_JB: {
            state.current_instruction.type = JB;
            conditional_jump(&state);
        } break;

        case OP_JBE: {
            state.current_instruction.type = JBE;
            conditional_jump(&state);
        } break;

        case OP_JP: {
            state.current_instruction.type = JP;
            conditional_jump(&state);
        } break;

        case OP_JO: {
            state.current_instruction.type = JO;
            conditional_jump(&state);
        } break;

        case OP_JS: {
            state.current_instruction.type = JS;
            conditional_jump(&state);
        } break;

        case OP_JNE: {
            state.current_instruction.type = JNE;
            conditional_jump(&state);
        } break;

        case OP_JNL: {
            state.current_instruction.type = JNL;
            conditional_jump(&state);
        } break;

        case OP_JG: {
            state.current_instruction.type = JG;
            conditional_jump(&state);
        } break;

        case OP_JNB: {
            state.current_instruction.type = JNB;
            conditional_jump(&state);
        } break;

        case OP_JA: {
            state.current_instruction.type = JA;
            conditional_jump(&state);
        } break;

        case OP_JNP: {
            state.current_instruction.type = JNP;
            conditional_jump(&state);
        } break;

        case OP_JNO: {
            state.current_instruction.type = JNO;
            conditional_jump(&state);
        } break;

        case OP_JNS: {
            state.current_instruction.type = JNS;
            conditional_jump(&state);
        } break;

        case OP_JCXZ: {
            state.current_instruction.type = JCXZ;
            conditional_jump(&state);
        } break;

        case OP_LOOP: {
            state.current_instruction.type = LOOP;
            conditional_jump(&state);
        } break;

        case OP_LOOPZ: {
            state.current_instruction.type = LOOPZ;
            conditional_jump(&state);
        } break;

        case OP_LOOPNZ: {
            state.current_instruction.type = LOOPNZ;
            conditional_jump(&state);
        } break;

        case OP_NOP: {
            state.current_instruction.type = NOP;
            consume_u8(&state);
        } break;

        case OP_INVALID: {
            invalid_opcode_abort(instruction_byte);
        }
        }

        add_instruction(&state);
    }

    return state.first_instruction;
}

void
add_instruction(simstate_t *state)
{
    // TODO: Should we assert that at least some data has been added to
    // the current instruction?
    //ASSERT(! IS_NULL_INSTRUCTION(state.current_instruction));
    //
    inst_dis_t *result = POOL_PUSH_STRUCT(inst_dis_t, state->pool);
    *result = state->current_instruction;

    state->current_instruction = (inst_dis_t) {};

    if (state->first_instruction)
        state->last_instruction->next = result;
    else
        state->first_instruction = result;

    state->last_instruction = result;
}

void
print_disassembly(inst_dis_t *instructions, char *binname)
{
    int binname_len = strnlen(binname, MAX_STRING_LEN);
    printf("; %.*s%s disassembly:\n"
           "bits 16\n",
            binname_len, binname,
            binname_len == MAX_STRING_LEN ? "..." : ""
    );

    while (instructions)
    {
        printf("%.*s",
                (int)mnemonics[instructions->type].sz,
                mnemonics[instructions->type].text
        );

        if (! IS_NULL_OPERAND(instructions->dst))
        {
            printf(" ");
            print_operand(&instructions->dst);
        }

        if (! IS_NULL_OPERAND(instructions->src))
        {
            printf(", ");
            print_operand(&instructions->src);
        }

        putchar('\n');
        instructions = instructions->next;
    }
}

void print_operand(operand_t *o)
{
    switch (o->type)
    {
    case REG: {
        // Index into Register strings
        printf("%.*s", (int)reg_strings[o->idx].sz, reg_strings[o->idx].text);
    } break;

    case MEM: {
        // Index into Effective Address strings
        putchar('[');
        printf("%.*s", (int)ea_strings[o->idx].sz, ea_strings[o->idx].text);
        if (o->disp)
            printf(" + %hu", o->disp);
        putchar(']');
    } break;

    case IMM: {
        // Immediate value
        printf("%hu", o->data);
    } break;

    case ADR: /* fall through */
    case DRA: {
        // Direct Address value
        printf("[%hu]", o->disp);
    } break;

    case EXP: {
        printf("%.*s %hu", 4, o->is_word ? "word" : "byte", o->data);
    } break;

    case INC8: {
        printf("$+2%+hd", o->inc8);
    } break;

    default: {
        /* Something went wrong */
        // TODO: Do something about it?
    } break;
    }
}

/*
 * Returns the resulting full instruction length in bytes
 */
void
op_reg_rm(simstate_t *state)
{
    // REG_RM is a two to four byte instruction

    u8 current_byte = consume_u8(state);

    u8 dbit = DBIT(current_byte);
    u8 wbit = WBIT(current_byte);

    current_byte = consume_u8(state);

    u8 mod  = MOD(current_byte);
    u8 reg  = REG(5, current_byte);
    u8 rm   = RM(current_byte);

    operand_t *reg_out;
    operand_t *rm_out;

    if (dbit)
    {
        reg_out = &state->current_instruction.dst;
        rm_out  = &state->current_instruction.src;
    }
    else
    {
        reg_out = &state->current_instruction.src;
        rm_out  = &state->current_instruction.dst;
    }

    reg_out->type = REG;
    reg_out->idx  = REGIDX(reg, wbit);

    if (mod == REG_MODE)  // register to register
    {
        rm_out->type = REG;
        rm_out->idx  = REGIDX(rm, wbit);
    }
    else  // memory to/from register
    {
        handle_memory_operand(state, mod, rm, rm_out);
    }
}

void
mov_reg_imm(simstate_t *state)
{
    u8 current_byte = consume_u8(state);
    u8 wbit = BIT(3, current_byte);
    u8 reg  = REG(2, current_byte);

    state->current_instruction.src.type = IMM;
    state->current_instruction.src.data = wbit ? consume_u16(state) : consume_u8(state);

    state->current_instruction.dst.type = REG;
    state->current_instruction.dst.idx  = REGIDX(reg, wbit);
}

void
mov_rm_imm(simstate_t *state)
{
    u8 current_byte = consume_u8(state);

    u8 wbit = WBIT(current_byte);

    current_byte = consume_u8(state);

    u8 mod  = MOD(current_byte);
    u8 rm   = RM(current_byte);

    operand_t *out = &state->current_instruction.dst;
    operand_t *imm = &state->current_instruction.src;

    // MOV_REG_IMM is three to six bytes
    //u8 instruction_len = 3 + wbit;
    // The immediate value for this instruction will be in the third byte minimum
    //u8 imm_offset      = 2;

    if (mod == REG_MODE)
    {
        out->type = REG;
        out->idx  = REGIDX(rm, wbit);
        imm->type = IMM;
    }
    else
    {
        handle_memory_operand(state, mod, rm, out);
        imm->type = EXP;
        imm->is_word = wbit;
    }

    imm->data    = wbit ? consume_u16(state) : consume_u8(state);
}

/*
 * This handles both MOV_ACC_MEM and MOV_MEM_ACC
 */
void
mov_acc_to_from_mem(simstate_t *state)
{
    // MOV_MEM_ACC is always three bytes even when wbit is set and AL is used
    u8 current_byte = consume_u8(state);

    // IMPORTANT: This is not documented usage but bit 1 is effectively a
    // "direction" bit; similar to the d-bit in op_reg_rm but flipped.
    u8 to_acc = !BIT(1, current_byte);
    u8 wbit   = WBIT(current_byte);

    operand_t *acc;
    operand_t *mem;

    if (to_acc)
    {
        mem = &state->current_instruction.src;
        acc = &state->current_instruction.dst;
    }
    else
    {
        acc = &state->current_instruction.src;
        mem = &state->current_instruction.dst;
    }

    // The register is AX or AL
    acc->type = REG;
    acc->idx  = REGIDX(0, wbit);

    mem->type = ADR;
    mem->disp = wbit ? consume_u16(state) : consume_u8(state);
}

void
asc_rm_imm(simstate_t *state)
{
    u8 current_byte = consume_u8(state);

    u8 wbit = WBIT(current_byte);
    u8 sbit = SBIT(current_byte);

    current_byte = consume_u8(state);

    u8 op = SEC_OP(current_byte);
    if      (op == 00) state->current_instruction.type = ADD;
    else if (op == 05) state->current_instruction.type = SUB;
    else if (op == 07) state->current_instruction.type = CMP;

    u8 mod  = MOD(current_byte);
    u8 rm   = RM(current_byte);

    operand_t *out = &state->current_instruction.dst;
    operand_t *imm = &state->current_instruction.src;

    if (mod == REG_MODE)
    {
        out->type = REG;
        out->idx  = REGIDX(rm, wbit);
        imm->type = IMM;
    }
    else
    {
        handle_memory_operand(state, mod, rm, out);
        imm->type = EXP;
        imm->is_word = wbit;
    }

    imm->data = consume_s16(state, sbit && wbit, !sbit && wbit);
}

void
asc_acc_imm(simstate_t *state)
{
    u8 current_byte = consume_u8(state);

    u8 wbit   = WBIT(current_byte);

    operand_t *imm = &state->current_instruction.src;
    operand_t *acc = &state->current_instruction.dst;

    // The register is AX or AL
    acc->type = REG;
    acc->idx  = REGIDX(0, wbit);

    imm->type = IMM;
    imm->data = wbit ? consume_u16(state) : consume_u8(state);
}

void
conditional_jump(simstate_t *state)
{
    // We can ignore the opcode byte
    consume_u8(state);

    // NOTE: The manual refers to this as an 8-bit signed increment, though
    // being signed, it can technically be used to decrement as well.

    state->current_instruction.dst.type = INC8;
    state->current_instruction.dst.inc8 = consume_s8(state);
}

void
handle_memory_operand(simstate_t *state, u8 mod, u8 mem, operand_t *out)
{
    if (! (mod == MOD_NODISP && mem == DIRECT_ADDRESS))
    {
        out->type = MEM;
        out->idx  = mem;

        if (mod != MOD_NODISP)  // mod == MOD_DISP8 || mod == MOD_DISP16
            out->disp = consume_s16(state, mod == MOD_DISP8, mod == MOD_DISP16);
    }
    else
    {
        // DIRECT_ADDRESS always has 16-bit displacement
        out->type = DRA;
        out->disp = consume_u16(state);
    }
}

/*
 * Under certain conditions a single byte value should be sign extended,
 * and in others a second byte should be shifted into the hi byte position.
 *
 * In others still, we simply return the value of a single byte.
 */
s16
consume_s16(simstate_t *state, b8 should_sign_extend, b8 should_shift_hi_byte)
{
    u16 value = consume_u8(state);
    if (value)
    {
        if (should_sign_extend && (value & 0x80))
            // Sign-extend DISP-LO
            value |= 0xFF00;
        else if (should_shift_hi_byte)
            // Shift in DISP-HI
            value |= consume_u8(state) << 8;

    }
    return value;
}

void
invalid_opcode_abort(u8 opcode)
{
    LOG_ERROR("Invalid opcode 0x%.2X. Aborting.", opcode);
    exit(1);
}

b8
bytes_remain(simstate_t *state)
{
    return state->bytecode_at < state->bytecode_end;
}

u8
peek_u8(simstate_t *state)
{
    return *state->bytecode_at;
}

u8
consume_u8(simstate_t *state)
{
    return *state->bytecode_at++;
}

s8
consume_s8(simstate_t *state)
{
    return *state->bytecode_at++;
}

u16
consume_u16(simstate_t *state)
{
    u16 result = *(u16 *)state->bytecode_at;
    state->bytecode_at += 2;
    return result;
}

