#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "simdisasm.h"
#include "simpool.h"
#include "simcommon.h"


bytebuf_t *read_entire_file(char *filename, pool_t *pool);
void usage(char *proc);


int
main(int argc, char **argv)
{
    if (argc != 2)
    {
        usage(argv[0]);
        exit(1);
    }

    /* Setup */

    char *binname = argv[1];

    pool_t pool = pool_alloc(MB(4));

    if (NULL_POOL(&pool))
    {
        LOG_ERROR("Failed to allocate pool memory. Aborting.");
        exit(1);
    }

    bytebuf_t *binbuf = read_entire_file(binname, &pool);

    if (NULL_BYTE_BUF(binbuf))
    {
        LOG_ERROR("Failed to read binary file into process buffer. Aborting.");
        exit(1);
    }

    /* Do work */

    inst_dis_t *disasm = disassemble(binbuf, &pool);

    if (! disasm)
    {
        LOG_ERROR("Failed disassembly. Aborting.");
        exit(1);
    }

    print_disassembly(disasm, binname);

#ifndef NDEBUG
    /* Clean up */
    pool_release(&pool);
#endif

    return 0;
}

/*
 * Returns a null bytebuf_t on error
 */
bytebuf_t *
read_entire_file(char *filename, pool_t *pool)
{
    FILE *f = fopen(filename, "r");

    if (! f)
        return NULL;

    fseek(f, 0, SEEK_END);
    size_t flen = ftell(f);
    rewind(f);

    bytebuf_t *buf = POOL_PUSH_STRUCT(bytebuf_t, pool);
    buf->bytes = POOL_PUSH(flen, pool);
    buf->sz = flen;

    if (fread(buf->bytes, 1, flen, f) < flen)
        return NULL;

    fclose(f);

    return buf;
}

void
usage(char *proc)
{
    fprintf(stderr, "Usage: %s asm-file\n", proc);
}
